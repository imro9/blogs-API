const express = require("express");
const jwt = require("jsonwebtoken");
const router = express.Router();
const { users } = require("../db");
const { registValidation, handleValidationErrors, loginValidation, createUser } = require("../middleware/middleware");
const { User } = require("../middleware/class");

router.get("/", (req, res) => {
  res.status(200).json(users);
});

router.post("/register", registValidation, handleValidationErrors, (req, res) => {
  const { email } = req.body;
  if (users.length < 1) {
    const user_id = 1;
    createUser(user_id, req);
    return res.status(201).json({ status: "success", message: "Created User" });
  }
  const checkEmail = users.findIndex((user) => user.validationEmail(email));
  if (checkEmail === -1) {
    const user_id = users[users.length - 1].user_id + 1;
    createUser(user_id, req);
    return res.status(201).json({ status: "success", message: "Created User" });
  } else {
    res.status(400).json({ status: "fail", message: "Email already registed" });
  }
});

router.post("/login", loginValidation, handleValidationErrors, (req, res) => {
  const { email, password } = req.body;
  const user = users.find((user) => user.validationEmail(email));
  if (!user) {
    return res.status(401).json({ status: "fail", message: "Cannot match with any email registed" });
  }
  if (user.validationPassword(password)) {
    const token = jwt.sign({ email }, "secretkey", { expiresIn: "1m" });
    return res.status(200).json({ status: "success", message: "Login Success!", token });
  }
  res.status(401).json({ status: "fail", message: "wrong password" });
});

// { expiresIn: "1m" }
module.exports = { router, users };
