const express = require("express");
const router = express.Router();
const { users } = require("../db");
const { blogs } = require("../db");
const { verifyToken, postBodyValidation, handleValidationErrors, valueBodyValidation, queryValidation } = require("../middleware/middleware");
const { Blog } = require("../middleware/class");

router.post("/", verifyToken, postBodyValidation, valueBodyValidation, handleValidationErrors, (req, res) => {
  const email = req.email;
  const user = users.find((user) => user.validationEmail(email));
  const { title, content } = req.body;
  const blog_id = blogs.length < 1 ? 1 : blogs[blogs.length - 1].blog_id + 1;
  const blog = new Blog(user.name, user.user_id, blog_id, title, content);
  blogs.push(blog);
  user.addBlog({ blog_id, title });
  res.status(200).json({ status: "success", message: "success add blog" });
});

router.get("/", (req, res) => {
  res.status(200).json({ blogs: blogs });
});

router.put("/like", queryValidation, (req, res) => {
  const blog = req.blog;
  blog.addLike();
  res.status(200).json(blog);
});

module.exports = { router, blogs };
