const express = require("express");
const app = express();
const { router: userRouters } = require("./routes/users");
const { router: postRouters } = require("./routes/posts");
const { internalErrorHandler, notFoundErrorHandler } = require("./middleware/middleware");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/user", userRouters);
app.use("/posts", postRouters);
app.use(internalErrorHandler);
app.use(notFoundErrorHandler);

app.listen(3000);
