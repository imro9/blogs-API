class User {
  #password;
  #email;
  constructor(user_id, name) {
    this.user_id = user_id;
    this.name = name;
    this.blogs = [];
  }
  setAccount(email, password) {
    this.#email = email;
    this.#password = password;
  }

  validationEmail(email) {
    return email.toLowerCase() === this.#email;
  }

  validationPassword(password) {
    return password == this.#password;
  }
  addBlog(blog) {
    this.blogs.push(blog);
  }
}

class Blog {
  constructor(name, user_id, blog_id, title, content) {
    this.name = name;
    this.user_id = user_id;
    this.blog_id = blog_id;
    this.title = title;
    this.content = content;
    this.like = 0;
  }

  addLike() {
    this.like += 1;
  }
}

module.exports = { User, Blog };
