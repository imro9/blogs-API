const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const { blogs } = require("../db");
const { users } = require("../db");
const { User } = require("./class");

const registValidation = [
  check("name").notEmpty().withMessage("name cannot be empty"),
  check("email").notEmpty().withMessage("email cannot be empty").isEmail().withMessage("email invalid"),
  check("password").notEmpty().withMessage("password cannot be empty").isLength({ min: 6 }).withMessage("password is too short"),
];

const postBodyValidation = [check("title").notEmpty().withMessage("title cannot be empty"), check("content").notEmpty().withMessage("content cannot be empty")];

const loginValidation = [check("email").notEmpty().withMessage("Please insert email!"), check("password").notEmpty().withMessage("Please insert password!")];

function handleValidationErrors(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  next();
}

function verifyToken(req, res, next) {
  const token = req.headers["authorization"];
  if (typeof token !== "undefined") {
    jwt.verify(token, "secretkey", (err, authData) => {
      if (err) {
        res.sendStatus(403);
      } else {
        req.email = authData.email;
        next();
      }
    });
  } else {
    res.sendStatus(403);
  }
}

function internalErrorHandler(err, req, res, next) {
  console.log(err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
}

function notFoundErrorHandler(req, res, next) {
  res.status(404).json({
    status: "fail",
    errors: "Not Found",
  });
}

function valueBodyValidation(req, res, next) {
  const { title } = req.body;
  const checkTitle = blogs.findIndex((blog) => blog.title.toLowerCase() == title.toLowerCase());
  if (checkTitle === -1) {
    next();
  } else {
    res.sendStatus(400);
  }
}
function queryValidation(req, res, next) {
  const blogid = req.query.blogid;
  const blog = blogs.find((blog) => blog.blog_id == blogid);
  if (typeof blogid == "undefined" || typeof blog == "undefined") {
    return res.status(400).json({ status: "fail", message: "Cannot find any Blog" });
  }
  req.blog = blog;
  next();
}

function createUser(user_id, req) {
  const { name, email, password } = req.body;
  const user = new User(user_id, name);
  user.setAccount(email, password);
  users.push(user);
}
module.exports = { registValidation, queryValidation, createUser, valueBodyValidation, handleValidationErrors, loginValidation, verifyToken, postBodyValidation, internalErrorHandler, notFoundErrorHandler };
